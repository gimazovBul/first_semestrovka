package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private Integer id;
    private String name;
    private Author author;
    private Integer year;
    private Integer pageAmount;
    private Subject subject;
    private Float rating;
    private Language lang;
    private String about;
    private String path;
    private Integer totalRating;
    private Integer ratingCount;
    private String image;

}
