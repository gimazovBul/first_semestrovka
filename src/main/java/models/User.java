package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String nickname;
    private String email;
    private Country country;
    private Boolean adult;
    private Boolean deleted;
    private String avatar;
    private String password;
    private String name;

    public User(String nickname, String email, Country country,
                Boolean adult, Boolean deleted, String avatar, String name, String password) {
        this.nickname = nickname;
        this.email = email;
        this.country = country;
        this.adult = adult;
        this.deleted = deleted;
        this.avatar = avatar;
        this.name = name;
        this.password = password;
    }
}
