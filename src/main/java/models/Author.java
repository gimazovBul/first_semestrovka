package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    private Integer id;
    private String name;
    private Country country;
    private Subject subject;
    private Float rating;
    private Integer totalRating;
    private Integer ratingCount;
    private String image;
}
