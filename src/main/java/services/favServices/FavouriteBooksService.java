package services.favServices;

import models.Book;
import models.User;
import repositories.entittyRepositories.BookRepository;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavBooksRepository;
import services.entityServices.BookService;
import services.entityServices.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class FavouriteBooksService implements FavBooksRepository {
    private Connection connection;
    private UserRepository userService;
    private BookRepository bookService;

    public FavouriteBooksService(Connection connection) {
        this.connection = connection;
        userService = new UserService(connection);
        bookService = new BookService(connection);
    }

    @Override
    public void saveToFav(Integer userId, Integer bookId) {
        String sqlToSave = "INSERT INTO first_semester_work_scheme.fav_books (user_id, book_id) " +
                "VALUES (?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(sqlToSave)) {
            ps.setInt(1, userId);
            ps.setInt(2, bookId);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new IllegalStateException();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void deleteFromFav(Integer userId, Integer bookId) {
        String sqlToDelete = "DELETE FROM first_semester_work_scheme.fav_books WHERE user_id = ? AND book_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlToDelete)) {
            ps.setInt(1, userId);
            ps.setInt(2, bookId);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<List<Book>> getAllFav(Integer userId) {
        String sql = "SELECT book_id FROM first_semester_work_scheme.fav_books WHERE user_id = ?";
        List<Book> favBooks = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                favBooks.add(bookService.find(rs.getInt("book_id")).get());
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (favBooks.size() == 0) return Optional.empty();
        return Optional.of(favBooks);
    }
    public boolean isFav(Integer userId, Integer bookId){
        String sql = "SELECT user_id, book_id FROM first_semester_work_scheme.fav_books" +
                " WHERE EXISTS(SELECT user_id, book_id WHERE user_id = ? AND book_id = ?)";
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setInt(2, bookId);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) return true;
            else return false;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
