package services.favServices;

import models.Author;
import repositories.entittyRepositories.AuthorRepository;
import repositories.favRepositories.FavAuthorsRepository;
import services.entityServices.AuthorService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FavoriteAuthorsService implements FavAuthorsRepository {
    private Connection connection;

    public FavoriteAuthorsService(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void saveToFav(Integer userId, Integer authorId) {
        String sql = "INSERT INTO first_semester_work_scheme.fav_authors (user_id, author_id) VALUES (?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setInt(2, authorId);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void deleteFromFav(Integer userId, Integer authorId) {
        String sql = "DELETE FROM first_semester_work_scheme.fav_authors WHERE user_id = ? AND author_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setInt(2, authorId);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Optional<List<Author>> getAllFav(Integer userId) {
        String sql = "SELECT author_id FROM first_semester_work_scheme.fav_authors WHERE user_id = ?";
        List<Author> authors = new ArrayList<>();
        AuthorRepository authorService = new AuthorService(connection);
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                System.out.println(authorService.find(resultSet.getInt("author_id")).get());
                authors.add(authorService.find(resultSet.getInt("author_id")).get());
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (authors.size() == 0) return Optional.empty();
        else return Optional.of(authors);
    }

    public boolean isFav(Integer userId, Integer authorId){
        String sql = "SELECT user_id, author_id FROM first_semester_work_scheme.fav_authors" +
                " WHERE EXISTS(SELECT user_id, author_id WHERE user_id = ? AND author_id = ?)";
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setInt(2, authorId);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) return true;
            else return false;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
