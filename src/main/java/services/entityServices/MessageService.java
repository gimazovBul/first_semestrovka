package services.entityServices;


import models.Message;
import models.User;
import repositories.entittyRepositories.MessageRepository;
import repositories.entittyRepositories.UserRepository;
import services.RowMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


public class MessageService implements MessageRepository {
    private Connection connection;
    private UserRepository userService;

    private RowMapper<Message> messageRowMapper = row -> {
        Integer id = row.getInt("id");
        User sender = userService.find(row.getInt("id1")).get();
        User receiver = userService.find(row.getInt("id2")).get();
        String text = row.getString("text");
        Date date = new Date(row.getTimestamp("date").getTime());
        return new Message(id, sender, receiver, text, date);
    };

    public MessageService(Connection connection) {
        this.connection = connection;
        userService = new UserService(connection);
    }

    @Override
    public Optional<List<Message>> findImById(Integer id) {
        String sqlToFindIm = "SELECT * FROM first_semester_work_scheme.messages WHERE id2 = ? ORDER BY date desc ";
        List<Message> messages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sqlToFindIm)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                messages.add(messageRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (messages.size() == 0) return Optional.empty();
        return Optional.of(messages);
    }

    @Override
    public void save(Message message) {
        String sqlToSave = "INSERT INTO first_semester_work_scheme.messages" +
                " (id1, id2, text, date) VALUES (?, ?, ?, ?)";
        Integer senderId = message.getSender().getId();
        Integer receiverId = message.getReceiver().getId();
        String text = message.getText();
        Long date = message.getDate().getTime();
        try (PreparedStatement ps = connection.prepareStatement(sqlToSave)) {
            ps.setInt(1, senderId);
            ps.setInt(2, receiverId);
            ps.setString(3, text);
            ps.setTimestamp(4, new Timestamp(date));
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Message> find(Integer id) {
        String sql = "SELECT * FROM first_semester_work_scheme.messages WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) return Optional.of(messageRowMapper.mapRow(resultSet));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<List<Message>> findOmById(Integer id) {
        String sqlToFindIm = "SELECT * FROM first_semester_work_scheme.messages WHERE id1 = ? order by date desc ";
        List<Message> messages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sqlToFindIm)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                messages.add(messageRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (messages.size() == 0) return Optional.empty();
        return Optional.of(messages);

    }

    @Override
    public Optional<List<Message>> findDialog(Integer id1, Integer id2) {
        String sqlToFindDialog = "SELECT * FROM first_semester_work_scheme.messages WHERE (id1 = ? AND id2 = ?)" +
                "OR (id1 = ? AND id2 = ?)";
        List<Message> messages = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sqlToFindDialog)) {
            ps.setLong(1, id1);
            ps.setLong(2, id2);
            ps.setLong(3, id2);
            ps.setLong(4, id1);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                messages.add(messageRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (messages.size() == 0) return Optional.empty();
        return Optional.of(messages);
    }

}
