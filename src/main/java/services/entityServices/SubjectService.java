package services.entityServices;

import models.Subject;
import repositories.entittyRepositories.SubjectRepository;
import services.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class SubjectService implements SubjectRepository {
    private Connection connection;

    RowMapper<Subject> rowMapper = row -> {
        Integer id = row.getInt("id");
        String name = row.getString("name");
        Boolean adult = row.getBoolean("adult");
        return new Subject(id, name, adult);
    };

    public SubjectService(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Subject> findById(Integer id) {
        String sql = "SELECT * FROM first_semester_work_scheme.subjects WHERE id = ?";
        Subject subject = null;
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) subject = rowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(subject);
    }
}
