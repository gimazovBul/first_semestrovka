package services.entityServices;

import models.Country;
import models.User;
import org.mindrot.jbcrypt.BCrypt;
import repositories.entittyRepositories.CountryRepository;
import repositories.entittyRepositories.UserRepository;
import services.RowMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class UserService implements UserRepository {
    private Connection connection;
    private CountryRepository countryService;

    public UserService(Connection connection) {
        this.connection = connection;
        this.countryService = new CountryService(connection);
    }

    private RowMapper<User> rowMapper = row -> {
        Integer id = row.getInt("id");
        String nickname = row.getString("nickname");
        String email = row.getString("email");
        Country country = countryService.findById(row.getInt("country_id")).get();
        Boolean adult = row.getBoolean("adult");
        Boolean deleted = row.getBoolean("deleted");
        String avatar = row.getString("avatar");
        String name = row.getString("name");
        String password = row.getString("password");
        return new User(id, nickname, email, country, adult, deleted, avatar, password, name);
    };

    @Override
    public User save(User user) {
        if (user == null) throw new NullPointerException("User can't be null");
        String SQL_INSERT = "INSERT INTO " +
                "first_semester_work_scheme.users (nickname, email," +
                " country_id, adult, deleted, avatar, password, name) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getNickname());
            ps.setString(2, user.getEmail());
            ps.setInt(3, user.getCountry().getId());
            ps.setBoolean(4, user.getAdult());
            ps.setBoolean(5, false);
            ps.setString(6, user.getAvatar());
            ps.setString(7, user.getPassword());
            ps.setString(8, user.getName());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException();
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getInt("id"));
                    return user;
                } else {
                    throw new SQLException();
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(User user) {
        if (user == null) throw new NullPointerException("User can't be null");
        String sql_Update = "UPDATE first_semester_work_scheme.users SET nickname = ?," +
                " email = ?, country_id = ?, name = ?, deleted = ?, avatar = ? WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql_Update, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, user.getNickname());
            ps.setString(2, user.getEmail());
            ps.setInt(3, user.getCountry().getId());
            ps.setString(4, user.getName());
            ps.setBoolean(5, user.getDeleted());
            ps.setString(6, user.getAvatar());
            ps.setInt(7, user.getId());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        Optional<User> userOptional = find(id);
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
        } else throw new NoSuchElementException();
        String sqlDelete = "UPDATE first_semester_work_scheme.users SET deleted = true WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlDelete)) {
            ps.setInt(1, id);
            user.setDeleted(true);
            int executedRows = ps.executeUpdate();
            if (executedRows == 0)
                throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> find(Integer id) {
        User user = null;
        String sql_Find = "SELECT * FROM first_semester_work_scheme.users WHERE id = ?;";

        try (PreparedStatement ps = connection.prepareStatement(sql_Find)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                user = rowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(user);

    }

    @Override
    public Optional<List<User>> findAll() {
        List<User> result = new ArrayList<>();
        String sqlFindAll = "SELECT * FROM first_semester_work_scheme.users WHERE deleted = false;";
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sqlFindAll)) {
            while (resultSet.next()) {
                User user = rowMapper.mapRow(resultSet);
                result.add(user);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (result.size() == 0) return Optional.empty();
        return Optional.of(result);
    }

    @Override
    public Optional<User> findByNickname(String nickname) {
        User user = null;
        String sql_Find = "SELECT * FROM first_semester_work_scheme.users WHERE nickname = ?"
                + " AND deleted = false";

        try (PreparedStatement ps = connection.prepareStatement(sql_Find)) {
            ps.setString(1, nickname);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                user = rowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        User user = null;
        String sql_Find = "SELECT * FROM first_semester_work_scheme.users WHERE email = ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql_Find)) {
            ps.setString(1, email);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                user = rowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(user);
    }


    public Optional<User> checkEmailPassword(String email, String password) {
        String sql = "SELECT * FROM first_semester_work_scheme.users WHERE email = ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = rowMapper.mapRow(rs);
                if (BCrypt.checkpw(password, user.getPassword())) {
                    return Optional.of(user);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.empty();
    }

    public Optional<User> checkNicknamePassword(String nickname, String password) {
        String sql = "SELECT * FROM first_semester_work_scheme.users WHERE nickname = ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, nickname);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = rowMapper.mapRow(rs);
                if (BCrypt.checkpw(password, user.getPassword())) {
                    return Optional.of(user);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.empty();
    }

    public Optional<List<User>> findByLikePattern(String pattern){
        String sql = "SELECT * FROM first_semester_work_scheme.users WHERE nickname LIKE ?";
        List<User> users = new ArrayList<>();
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1, "%" + pattern + "%");
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                users.add(rowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (users.size() == 0)
            return Optional.empty();
        else return Optional.of(users);
    }
}
