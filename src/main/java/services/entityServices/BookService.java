package services.entityServices;

import models.Author;
import models.Book;
import models.Language;
import models.Subject;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.BookRepository;
import repositories.entittyRepositories.LanguageRepository;
import repositories.entittyRepositories.SubjectRepository;
import services.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookService implements BookRepository {
    private Connection connection;
    private SubjectRepository subjectService;
    private AuthorRepository authorService;
    private LanguageRepository languageService;

    private RowMapper<Book> bookRowMapper = row -> {
        Integer id = row.getInt("id");
        String name = row.getString("name");
        Author author = authorService.find(row.getInt("author_id")).get();
        Integer year = row.getInt("year");
        Integer pageAmount = row.getInt("page_amount");
        Subject subject = subjectService.findById(row.getInt("subject_id")).get();
        Language language = languageService.findById(row.getInt("lang_id")).get();
        Float rating = row.getFloat("rating");
        String about = row.getString("about");
        String path = row.getString("path");
        Integer totalRating = row.getInt("total_rating");
        Integer ratingCount = row.getInt("rating_count");
        String image = row.getString("image");
        return new Book(id, name, author, year, pageAmount, subject,
                rating, language, about, path, totalRating, ratingCount, image);
    };

    public BookService(Connection connection) {
        this.connection = connection;
        subjectService = new SubjectService(connection);
        authorService = new AuthorService(connection);
        languageService = new LanguageService(connection);
    }

    @Override
    public Optional<List<Book>> findAllByName(String name) {
        List<Book> books = new ArrayList<>();
        String sqlByName = "SELECT * FROM first_semester_work_scheme.books WHERE name = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlByName)) {
            ps.setString(1, name);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }

        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findAllByGenre(Integer genreId) {
        List<Book> books = new ArrayList<>();
        String sqlByGenre = "SELECT * FROM first_semester_work_scheme.books WHERE subject_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlByGenre)) {
            ps.setInt(1, genreId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findAllByAuthor(Integer authorId) {
        List<Book> books = new ArrayList<>();
        String sqlByAuthor = "SELECT * FROM first_semester_work_scheme.books WHERE author_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlByAuthor)) {
            ps.setInt(1, authorId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findAllByLangs(Integer languagesId) {
        List<Book> books = new ArrayList<>();
        String sqlByLang = "SELECT * FROM first_semester_work_scheme.books WHERE  lang_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlByLang)) {
            ps.setInt(1, languagesId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findAllByPageAmount(Integer min, Integer max) {
        List<Book> books = new ArrayList<>();
        String sqlByAmount = "SELECT * FROM first_semester_work_scheme.books " +
                "WHERE page_amount BETWEEN ? AND ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlByAmount)) {
            ps.setInt(1, min);
            ps.setInt(2, max);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findAllByRating(Integer min, Integer max) {
        List<Book> books = new ArrayList<>();
        String sqlByRating = "SELECT * FROM first_semester_work_scheme.books " +
                "WHERE rating BETWEEN ? AND ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlByRating)) {
            ps.setInt(1, min);
            ps.setInt(2, max);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findBest5() {
        String sql = "SELECT * FROM first_semester_work_scheme.books ORDER BY rating LIMIT 5;";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }


    @Override
    public Optional<Book> find(Integer id) {
        Book book = null;
        String sqlById = "SELECT * FROM first_semester_work_scheme.books WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlById)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                book = bookRowMapper.mapRow(resultSet);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(book);
    }

    @Override
    public Optional<List<Book>> findAll() {
        List<Book> books = new ArrayList<>();
        String sqlAll = "SELECT * FROM first_semester_work_scheme.books";
        try (PreparedStatement ps = connection.prepareStatement(sqlAll)) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public void update(Book book) {
        String sql = "UPDATE first_semester_work_scheme.books SET name = ?, author_id = ?," +
                " year = ?, page_amount = ?, subject_id = ?, lang_id = ?, rating = ?, about = ?," +
                "  path = ?, total_rating = ?, rating_count = ? WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, book.getName());
            ps.setInt(2, book.getAuthor().getId());
            ps.setInt(3, book.getYear());
            ps.setInt(4, book.getPageAmount());
            ps.setInt(5, book.getSubject().getId());
            ps.setInt(6, book.getLang().getId());
            ps.setFloat(7, book.getTotalRating() / book.getRatingCount());
            ps.setString(8, book.getAbout());
            ps.setString(9, book.getPath());
            ps.setInt(10, book.getTotalRating());
            ps.setInt(11, book.getRatingCount());
            ps.setInt(12, book.getId());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    //todo check, most likely need fixing
    @Override
    public Optional<List<Book>> findBest5OfAuthor(Integer id) {
        List<Book> books = new ArrayList<>();
        String sql = "SELECT * FROM first_semester_work_scheme.books" +
                " JOIN first_semester_work_scheme.authors" +
                " ON first_semester_work_scheme.books.id = first_semester_work_scheme.authors.id " +
                "ORDER BY first_semester_work_scheme.books.rating LIMIT 5;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                books.add(bookRowMapper.mapRow(rs));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0) return Optional.empty();
        return Optional.of(books);
    }

    @Override
    public Optional<List<Book>> findByLikePattern(String pattern) {
        String sql = "SELECT * FROM first_semester_work_scheme.books WHERE name LIKE ? LIMIT 5";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "%" + pattern + "%");
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                books.add(bookRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (books.size() == 0)
            return Optional.empty();
        else return Optional.of(books);
    }

    @Override
    public void rate(int userId, int bookId, int rate) {
        String sqlInsert = "INSERT INTO first_semester_work_scheme.book_rating (user_id, book_id, rate) VALUES (?, ?, ?)";
        String sqlUpdate = "UPDATE first_semester_work_scheme.book_rating SET user_id = ?, book_id = ?, rate = ?" +
                " WHERE user_id = ? AND book_id = ?";
        Integer rateIfExists = checkIfRated(userId, bookId);
        if (rateIfExists == null) {
            try (PreparedStatement ps = connection.prepareStatement(sqlInsert)) {
                ps.setInt(1, userId);
                ps.setInt(2, bookId);
                ps.setInt(3, rate);
                int affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException();
                else {
                    Book book;
                    Optional<Book> potentialBook = find(bookId);
                    if (potentialBook.isPresent()) {
                        book = potentialBook.get();
                        book.setTotalRating(book.getTotalRating() + rate);
                        book.setRatingCount(book.getRatingCount() + 1);
                        update(book);
                    }
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        } else {
            try (PreparedStatement ps = connection.prepareStatement(sqlUpdate)) {
                ps.setInt(1, userId);
                ps.setInt(2, bookId);
                ps.setFloat(3, rate);
                ps.setInt(4, userId);
                ps.setInt(5, bookId);
                int affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException();
                else {
                    Book book;
                    Optional<Book> potentialBook = find(bookId);
                    if (potentialBook.isPresent()) {
                        book = potentialBook.get();
                        book.setTotalRating(book.getTotalRating() - rateIfExists + rate);

                        update(book);
                    }
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private Integer checkIfRated(int userId, int bookId) {
        String sqlIfExists = "SELECT rate FROM first_semester_work_scheme.book_rating WHERE" +
                " EXISTS(SELECT rate FROM first_semester_work_scheme.book_rating WHERE book_id = ? AND user_id = ?) AND book_id = ? AND user_id=?";
        try (PreparedStatement ps = connection.prepareStatement(sqlIfExists)) {
            ps.setInt(1, bookId);
            ps.setInt(2, userId);
            ps.setInt(3, bookId);
            ps.setInt(4, userId);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("rate");

            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }
}
