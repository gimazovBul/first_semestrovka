package services.entityServices;

import com.sun.rowset.internal.Row;
import models.Country;
import models.Language;
import repositories.entittyRepositories.CountryRepository;
import repositories.entittyRepositories.LanguageRepository;
import services.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class LanguageService implements LanguageRepository {
    private Connection connection;

    private RowMapper<Language> rowMapper = row -> {
        Integer id = row.getInt("id");
        String name = row.getString("name");
        return new Language(id, name);
    };


    public LanguageService(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<Language> findById(Integer id) {
        String sql = "SELECT * FROM first_semester_work_scheme.langs WHERE id = ?";
        Language language = null;
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) language = rowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(language);
    }
}
