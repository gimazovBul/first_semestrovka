package services.entityServices;

import models.Country;
import models.Language;
import repositories.entittyRepositories.CountryRepository;
import repositories.entittyRepositories.LanguageRepository;
import services.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class CountryService implements CountryRepository {
    private Connection connection;
    private LanguageRepository languageService;

    public CountryService(Connection connection) {
        this.connection = connection;
        this.languageService = new LanguageService(connection);
    }


    RowMapper<Country> rowMapper = row -> {
        Integer id = row.getInt("id");
        String name = row.getString("name");
        Language language = languageService.findById(row.getInt("lang")).get();
        return new Country(id, name, language);
    };


    @Override
    public Optional<Country> findById(Integer id) {
        String sql = "SELECT * FROM first_semester_work_scheme.countries WHERE id = ?";
        Country country = null;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) country = rowMapper.mapRow(rs);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(country);
    }
}
