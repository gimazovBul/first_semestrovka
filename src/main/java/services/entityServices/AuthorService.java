package services.entityServices;

import models.Author;
import models.Country;
import models.Subject;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.CountryRepository;
import repositories.entittyRepositories.SubjectRepository;
import services.RowMapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AuthorService implements AuthorRepository {
    private Connection connection;
    private CountryRepository countryService;
    private SubjectRepository subjectService;

    private RowMapper<Author> authorRowMapper = row -> {
        Integer id = row.getInt("id");
        String name = row.getString("name");
        Country country = countryService.findById(row.getInt("country")).get();
        Subject subject = subjectService.findById(row.getInt("subject")).get();
        Float rating = row.getFloat("rating");
        Integer totalRating = row.getInt("total_rating");
        Integer ratingCount = row.getInt("rating_count");
        String image = row.getString("image");
        return new Author(id, name, country, subject, rating, totalRating, ratingCount, image);
    };

    public AuthorService(Connection connection) {
        this.connection = connection;
        countryService = new CountryService(connection);
        subjectService = new SubjectService(connection);
    }

    @Override
    public void update(Author author) {
        String sql = "UPDATE first_semester_work_scheme.authors SET name = ?, country = ?," +
                "subject = ?, rating = ?, total_rating = ?, rating_count = ? WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, author.getName());
            ps.setInt(2, author.getCountry().getId());
            ps.setInt(3, author.getSubject().getId());
            ps.setFloat(4, author.getTotalRating() / author.getRatingCount());
            ps.setInt(5, author.getTotalRating());
            ps.setInt(6, author.getRatingCount());
            ps.setInt(7, author.getId());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Author> findByName(String name) {
        String sql = "SELECT * FROM first_semester_work_scheme.authors WHERE name = ?;";
        Author author = null;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) author = authorRowMapper.mapRow(rs);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(author);
    }

    @Override
    public Optional<Author> find(Integer id) {
        String sql = "SELECT * FROM first_semester_work_scheme.authors WHERE id = ?;";
        Author author = null;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) author = authorRowMapper.mapRow(rs);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.ofNullable(author);
    }

    @Override
    public Optional<List<Author>> findAll() {
        String sql = "SELECT * FROM first_semester_work_scheme.authors;";
        List<Author> authors = new ArrayList<>();
        try (Statement st = connection.createStatement()) {
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                authors.add(authorRowMapper.mapRow(rs));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (authors.size() == 0) return Optional.empty();
        return Optional.of(authors);
    }

    @Override
    public Optional<List<Author>> findAllByRating(Integer min, Integer max) {
        String sql = "SELECT * FROM first_semester_work_scheme.authors WHERE rating BETWEEN ? AND ?;";
        List<Author> authors = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, min);
            ps.setInt(2, max);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) authors.add(authorRowMapper.mapRow(rs));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (authors.size() == 0) return Optional.empty();
        return Optional.of(authors);
    }

    @Override
    public Optional<List<Author>> findAllByLangs(List<Integer> languagesId) {
        String sql = "SELECT * FROM first_semester_work_scheme.authors WHERE id = ?";
        List<Author> authors = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            for (Integer id : languagesId) {
                ps.setInt(1, id);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) authors.add(authorRowMapper.mapRow(rs));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (authors.size() == 0) return Optional.empty();
        return Optional.of(authors);
    }

    @Override
    public Optional<List<Author>> findBest5Authors() {
        String sql = "SELECT * FROM first_semester_work_scheme.authors ORDER BY rating LIMIT 5";
        List<Author> authors = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                authors.add(authorRowMapper.mapRow(rs));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (authors.size() == 0) return Optional.empty();
        return Optional.of(authors);
    }

    public Optional<List<Author>> findByLikePattern(String pattern) {
        String sql = "SELECT * FROM first_semester_work_scheme.authors WHERE name LIKE ? LIMIT 5";
        List<Author> authors = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "%" + pattern + "%");
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                authors.add(authorRowMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (authors.size() == 0)
            return Optional.empty();
        else return Optional.of(authors);
    }

    @Override
    public void rate(int userId, int authorId, int rate) {
        String sqlInsert = "INSERT INTO first_semester_work_scheme.author_rating (user_id, author_id, rate) VALUES (?, ?, ?)";
        String sqlUpdate = "UPDATE first_semester_work_scheme.author_rating SET user_id = ?, author_id = ?, rate = ?" +
                " WHERE user_id = ? AND author_id = ?";
        Integer rateIfExists = checkIfRated(userId, authorId);
        if (rateIfExists == null) {
            try (PreparedStatement ps = connection.prepareStatement(sqlInsert)) {
                ps.setInt(1, userId);
                ps.setInt(2, authorId);
                ps.setInt(3, rate);
                int affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException();
                else {
                    Author author;
                    Optional<Author> potentialAuthor = find(authorId);
                    if (potentialAuthor.isPresent()) {
                        author = potentialAuthor.get();
                        author.setTotalRating(author.getTotalRating() + rate);
                        author.setRatingCount(author.getRatingCount() + 1);
                        update(author);
                    }
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        } else {
            try (PreparedStatement ps = connection.prepareStatement(sqlUpdate)) {
                ps.setInt(1, userId);
                ps.setInt(2, authorId);
                ps.setFloat(3, rate);
                ps.setInt(4, userId);
                ps.setInt(5, authorId);
                int affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException();
                else {
                    Author author;
                    Optional<Author> potentialAuthor = find(authorId);
                    if (potentialAuthor.isPresent()) {
                        author = potentialAuthor.get();
                        author.setTotalRating(author.getTotalRating() - rateIfExists + rate);
                        update(author);
                    }
                }
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private Integer checkIfRated(int userId, int authorId) {
        String sqlIfExists = "SELECT rate FROM first_semester_work_scheme.author_rating WHERE" +
                " EXISTS(SELECT rate FROM first_semester_work_scheme.author_rating WHERE author_id = ? AND user_id = ?) AND author_id = ? AND user_id=?";
        try (PreparedStatement ps = connection.prepareStatement(sqlIfExists)) {
            ps.setInt(1, authorId);
            ps.setInt(2, userId);
            ps.setInt(3, authorId);
            ps.setInt(4, userId);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("rate");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }
}