package services.other;

import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FollowsService implements FollowsRepository {

    private Connection connection;

    public FollowsService(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void follow(Integer user1Id, Integer user2Id) {
        String sql = "INSERT INTO first_semester_work_scheme.follows (user1, user2) VALUES (?,?);";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, user1Id);
            ps.setInt(2, user2Id);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void unfollow(Integer user1Id, Integer user2Id) {
        String sql = "DELETE FROM first_semester_work_scheme.follows WHERE user1 = ? AND user2 = ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, user1Id);
            ps.setInt(2, user2Id);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<List<User>> findFriends(Integer id) {
        String sqlToSubs = "SELECT user2 FROM first_semester_work_scheme.follows WHERE user1 = ?";
        List<Long> potentFriendsIds = new ArrayList<>();
        UserRepository userService = new UserService(connection);
        List<User> friends = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sqlToSubs)) {
            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                potentFriendsIds.add(resultSet.getLong("user2"));
            }
        } catch (
                SQLException e) {
            throw new IllegalStateException(e);
        }
        String sqlToFriends = "SELECT user1 FROM first_semester_work_scheme.follows WHERE user1 = ? AND user2 = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlToFriends)) {
            for (Long potId : potentFriendsIds) {
                ps.setLong(1, potId);
                ps.setLong(2, id);
                ResultSet resultSet = ps.executeQuery();
                if (resultSet.next()) {
                    Optional<User> userOptional = userService.find(resultSet.getInt("user1"));
                    userOptional.ifPresent(friends::add);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (friends.size() == 0) return Optional.empty();
        else return Optional.of(friends);

    }

    @Override
    public Optional<List<User>> findFollowers(Integer userId) {
        String sql = "SELECT user1 FROM first_semester_work_scheme.follows WHERE user2 = ?;";
        List<Integer> potentialFollowers = new ArrayList<>();
        UserRepository userService = new UserService(connection);
        List<User> followers = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) potentialFollowers.add(resultSet.getInt("user1"));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (potentialFollowers.size() != 0) {
            Optional<List<User>> potentialFriends = findFriends(userId);
            List<User> friends;
            List<Integer> friendsIds = new ArrayList<>();

            if (potentialFriends.isPresent()) {
                friends = potentialFriends.get();
                for (User u : friends) {
                    friendsIds.add(u.getId());
                }
                potentialFollowers.removeAll(friendsIds);
                for (int i : potentialFollowers) {
                    followers.add(userService.find(i).get());
                }
            }
        }
        if (followers.size() == 0) return Optional.empty();
        return Optional.of(followers);

    }

    @Override
    public Optional<List<User>> findFollows(Integer userId) {
        String sql = "SELECT user2 FROM first_semester_work_scheme.follows WHERE user1 = ?";
        List<Integer> potentialFollows = new ArrayList<>();
        List<User> follows = new ArrayList<>();
        UserRepository userService = new UserService(connection);

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) potentialFollows.add(resultSet.getInt("user2"));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (potentialFollows.size() != 0) {
            Optional<List<User>> potentialFriends = findFriends(userId);
            List<User> friends;
            List<Integer> friendsIds = new ArrayList<>();
            if (potentialFriends.isPresent()) {
                friends = potentialFriends.get();
                for (User u : friends) {
                    friendsIds.add(u.getId());
                }
                potentialFollows.removeAll(friendsIds);
                for (Integer i : potentialFollows) {
                    follows.add(userService.find(i).get());
                }
            }
        }
        if (follows.size() == 0) return Optional.empty();
        return Optional.of(follows);
    }

    public String findState(Integer userId, Integer otherUserId) {
        boolean follows12 = false;
        boolean follows21 = false;
        String sql = "SELECT user1, user2 FROM first_semester_work_scheme.follows WHERE " +
                "EXISTS(SELECT user1, user2 FROM first_semester_work_scheme.follows" +
                " WHERE user1 = ? AND user2 = ?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setInt(2, otherUserId);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) follows12 = true;
            PreparedStatement ps2 = connection.prepareStatement(sql);
            ps2.setInt(1, otherUserId);
            ps2.setInt(2, userId);
            ResultSet resultSet1 = ps2.executeQuery();
            if (resultSet1.next()) follows21 = true;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        if (follows12 && follows21) return "friends";
        if (follows12) return "follows12";
        else if (follows21) return "follows21";
        else return "none";
    }
}
