package services.other;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStreamReader;

public class PartReader {
    public String read(Part part) {
        if (part == null) return "";
        try {
            return read0(part);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private String read0(Part part) throws IOException {
        try (InputStreamReader reader = new InputStreamReader(part.getInputStream())) {
            StringBuilder stringBuilder = new StringBuilder();
            char[] buffer = new char[1024];
            for (int c = 0; (c = reader.read(buffer)) != -1; )
                stringBuilder.append(buffer, 0, c);
            return stringBuilder.toString();
        }
    }
}
