package services.cookieService;


import repositories.cookieRepositories.CookieRepository;
import repositories.entittyRepositories.UserRepository;
import security.AuthToken;
import services.entityServices.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class CookieService implements CookieRepository {
    private Connection connection;
    private UserRepository userService;

    public CookieService(Connection connection) {
        this.connection = connection;
        userService = new UserService(connection);
    }

    @Override
    public Optional<AuthToken> getToken(String selector) {
        String sql = "SELECT * FROM first_semester_work_scheme.remember_me_cookies WHERE selector = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, selector);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) return Optional.of(
                    new AuthToken(
                            rs.getInt("id"),
                            rs.getInt("user_id"),
                            rs.getString("selector"),
                            rs.getString("validator")
                    ));
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return Optional.empty();
    }

    @Override
    public void save(AuthToken authToken) {
        String sql = "INSERT INTO first_semester_work_scheme.remember_me_cookies" +
                " (user_id, selector, validator) VALUES (?, ?, ?);";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, authToken.getUserId());
            ps.setString(2, authToken.getSelector());
            ps.setString(3, authToken.getValidator());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(AuthToken authToken) {
        String sql = "UPDATE first_semester_work_scheme.remember_me_cookies" +
                " SET selector = ?, validator = ? WHERE id = ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, authToken.getSelector());
            ps.setString(2, authToken.getValidator());
            ps.setInt(3, authToken.getId());
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM first_semester_work_scheme.remember_me_cookies WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, id);
            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) throw new SQLException();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
