package repositories.entittyRepositories;

import models.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository {
    Optional<Author> findByName(String name);

    Optional<Author> find(Integer id);

    Optional<List<Author>> findAll();

    Optional<List<Author>> findAllByRating(Integer min, Integer max);

    Optional<List<Author>> findAllByLangs(List<Integer> languagesId);

    Optional<List<Author>> findBest5Authors();

    Optional<List<Author>> findByLikePattern(String pattern);

    void rate(int userId, int authorId, int rate);

    void update(Author author);


}
