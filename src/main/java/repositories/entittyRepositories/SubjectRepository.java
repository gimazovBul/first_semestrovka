package repositories.entittyRepositories;

import models.Subject;

import java.util.Optional;

public interface SubjectRepository {
    Optional<Subject> findById(Integer id);
}
