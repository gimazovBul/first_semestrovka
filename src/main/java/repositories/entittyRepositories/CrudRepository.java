package repositories.entittyRepositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<ID, T> {
    T save(T t);

    void update(T t);

    void delete(ID id);

    Optional<T> find(ID id);

    Optional<List<T>> findAll();

}
