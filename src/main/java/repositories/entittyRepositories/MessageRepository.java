package repositories.entittyRepositories;

import models.Message;

import java.util.List;
import java.util.Optional;

public interface MessageRepository {
    Optional<List<Message>> findImById(Integer id);

    void save(Message message);

    Optional<Message> find(Integer id);

    Optional<List<Message>> findOmById(Integer id);

    Optional<List<Message>> findDialog(Integer id1, Integer id2);

}
