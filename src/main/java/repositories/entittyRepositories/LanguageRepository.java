package repositories.entittyRepositories;

import models.Language;

import java.util.Optional;

public interface LanguageRepository {
    Optional<Language> findById(Integer id);
}
