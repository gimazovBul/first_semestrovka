package repositories.entittyRepositories;

import models.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {
    Optional<List<Book>> findAll();

    Optional<Book> find(Integer id);

    Optional<List<Book>> findAllByName(String name);

    Optional<List<Book>> findAllByGenre(Integer genreId);

    Optional<List<Book>> findAllByAuthor(Integer authorId);

    Optional<List<Book>> findAllByLangs(Integer languagesId);

    Optional<List<Book>> findAllByPageAmount(Integer min, Integer max);

    Optional<List<Book>> findAllByRating(Integer min, Integer max);

    Optional<List<Book>> findBest5OfAuthor(Integer id);

    Optional<List<Book>> findBest5();

    Optional<List<Book>> findByLikePattern(String pattern);

    void rate(int userId, int bookId, int rate);

    void update(Book book);


}
