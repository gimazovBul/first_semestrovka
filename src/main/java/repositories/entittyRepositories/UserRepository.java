package repositories.entittyRepositories;

import models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<Integer, User> {
    Optional<User> findByNickname(String nickname);

    Optional<User> findByEmail(String email);

    Optional<User> checkNicknamePassword(String nickname, String password);

    Optional<User> checkEmailPassword(String email, String password);

    Optional<List<User>> findByLikePattern(String pattern);
}
