package repositories.entittyRepositories;

import models.Country;

import java.util.Optional;

public interface CountryRepository {
    Optional<Country> findById(Integer id);
}
