package repositories.cookieRepositories;

import security.AuthToken;

import java.util.Optional;

public interface CookieRepository {
    Optional<AuthToken> getToken(String selector);
    void save(AuthToken authToken);
    void update(AuthToken authToken);
    void delete(Integer id);
}
