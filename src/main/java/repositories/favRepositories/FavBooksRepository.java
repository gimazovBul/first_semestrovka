package repositories.favRepositories;

import models.Book;

public interface FavBooksRepository extends FavRepository<Integer, Book> {
}
