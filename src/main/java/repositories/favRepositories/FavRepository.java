package repositories.favRepositories;

import java.util.List;
import java.util.Optional;

public interface FavRepository<ID, T> {
    void saveToFav(ID userId, ID itemId);
    void deleteFromFav(ID userId, ID itemId);
    Optional<List<T>> getAllFav(Integer userId);
    boolean isFav(Integer userId, Integer itemId);
}
