package repositories.other;

import models.User;

import java.util.List;
import java.util.Optional;

public interface FollowsRepository {
    void follow(Integer user1Id, Integer user2Id);
    void unfollow(Integer user1Id, Integer user2Id);
    Optional<List<User>> findFriends(Integer id);
    Optional<List<User>> findFollows(Integer userId);
    Optional<List<User>> findFollowers(Integer userId);
    String findState(Integer userId, Integer otherUserId);
}
