package servlets.pdfServlet;

import models.Book;
import repositories.entittyRepositories.BookRepository;
import services.entityServices.BookService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "pdf", urlPatterns = "/loadPdf")
public class PdfServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/pdf");
        String bookId = req.getParameter("bookId");
        BookRepository bookService = (BookService) getServletContext().getAttribute("bookService");
        Optional<Book> potentialBook = bookService.find(Integer.valueOf(bookId));
        if (potentialBook.isPresent()) {
            Book book = potentialBook.get();
            resp.setHeader("Content-Length", String.valueOf(new File(book.getPath()).length()));
            ServletOutputStream outputStream = resp.getOutputStream();
            BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(book.getPath()));
            int c;
            while ((c = inputStream.read()) != -1) {
                outputStream.write(c);
            }

        }
    }
}
