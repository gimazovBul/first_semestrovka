package servlets.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Message;
import models.User;
import repositories.entittyRepositories.MessageRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.MessageService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "imMessagesList", urlPatterns = "/getIm")
public class ImMessagesListServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        MessageRepository messageService = (MessageService)getServletContext().getAttribute("messageService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        User user = null;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            Optional<List<Message>> imMessages = messageService.findImById(user.getId());
            imMessages.ifPresent(messages -> req.setAttribute("messages", messages));
            req.getRequestDispatcher("/WEB-INF/templates/im.ftl").forward(req, resp);
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
