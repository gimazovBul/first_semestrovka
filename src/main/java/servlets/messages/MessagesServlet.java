package servlets.messages;

import models.User;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.Optional;

@WebServlet(name = "Messages", urlPatterns = "/messages")
public class MessagesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()) {
            User user = potentialUser.get();
            req.setAttribute("user", user);
            req.getRequestDispatcher("/WEB-INF/templates/messages.ftl").forward(req, resp);

        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }
}
