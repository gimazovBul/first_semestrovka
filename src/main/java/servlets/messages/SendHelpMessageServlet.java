package servlets.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Message;
import models.User;
import repositories.entittyRepositories.MessageRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.MessageService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@WebServlet(name = "sendHelpMessage", urlPatterns = "/sendHelp")
public class SendHelpMessageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        MessageRepository messageService = (MessageService) getServletContext().getAttribute("messageService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        User user;
        String helpText;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            helpText = req.getParameter("helpText");
            messageService.save(new Message(user, userService.findByEmail(String.valueOf(req.getParameter("adminEmail"))).get(),
                    helpText, new Date(System.currentTimeMillis())));
            resp.setContentType("text/json");
            String respMessage = "message sent to admin";
            resp.getWriter().write(mapper.writeValueAsString(respMessage));
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }
}
