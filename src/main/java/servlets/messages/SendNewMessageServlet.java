package servlets.messages;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Message;
import models.User;
import repositories.entittyRepositories.MessageRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.MessageService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@WebServlet(name = "sendNewMessage", urlPatterns = "/sendMess")
public class SendNewMessageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        ObjectMapper mapper = new ObjectMapper();
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        MessageRepository messageService = (MessageService) getServletContext().getAttribute("messageService");
        Optional<User> potentialSender = userService.find((Integer) (session.getAttribute("user")));
        Optional<User> potentialReceiver = userService.find(Integer.valueOf(req.getParameter("user")));
        if (potentialSender.isPresent() && potentialReceiver.isPresent()) {
            User sender = potentialSender.get();
            User receiver = potentialReceiver.get();
            String text = req.getParameter("text");
            Message message = new Message(sender, receiver, text, new Date(System.currentTimeMillis()));
            messageService.save(message);
            resp.setContentType("text/json");
            String respMessage = "message sent to " + receiver.getName();
            resp.getWriter().write(mapper.writeValueAsString(respMessage));
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
