package servlets.messages;

import models.Message;
import models.User;
import repositories.entittyRepositories.MessageRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.MessageService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Om", urlPatterns = "/messages/om")
public class OmMessagesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        MessageRepository messageService = (MessageService)getServletContext().getAttribute("messageService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        User user = null;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            Optional<List<Message>> omMessages = messageService.findOmById(user.getId());
            omMessages.ifPresent(messages -> req.setAttribute("messages", messages));
            req.getRequestDispatcher("/WEB-INF/templates/om.ftl").forward(req, resp);
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }

}
