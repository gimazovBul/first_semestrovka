package servlets.messages;

import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;
import services.other.FollowsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "NewMessage", urlPatterns = "/messages/new")
public class NewMessageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        FollowsRepository friendsService =(FollowsService)getServletContext().getAttribute("followService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()) {
            Optional<List<User>> potentialUsers = friendsService.findFriends(potentialUser.get().getId());
            potentialUsers.ifPresent(users -> req.setAttribute("users", users));
            req.getRequestDispatcher("/WEB-INF/templates/newMessage.ftl").forward(req, resp);

        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }

}
