package servlets.fav;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavAuthorsRepository;
import services.entityServices.UserService;
import services.favServices.FavoriteAuthorsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "AddAuthorToFav", urlPatterns = "/addAuthorToFav")
public class AddAuthorToFavServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FavAuthorsRepository favAuthorsService =
                (FavoriteAuthorsService) getServletContext().getAttribute("favAuthorsService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()) {
            User user = potentialUser.get();
            String authorId = req.getParameter("authorId");
            boolean state = favAuthorsService.isFav(user.getId(), Integer.valueOf(authorId));
            if (state) {
                state = false;
                favAuthorsService.deleteFromFav(user.getId(), Integer.valueOf(authorId));
            } else {
                state = true;
                favAuthorsService.saveToFav(user.getId(), Integer.valueOf(authorId));
            }
            ObjectMapper mapper = new ObjectMapper();
            resp.setContentType("text/json");
            resp.getWriter().write(mapper.writeValueAsString(state));
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
