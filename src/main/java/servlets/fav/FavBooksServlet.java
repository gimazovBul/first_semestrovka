package servlets.fav;

import models.Book;
import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavBooksRepository;
import services.entityServices.UserService;
import services.favServices.FavouriteBooksService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "FavBooks", urlPatterns = "/favBooks")
public class FavBooksServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FavBooksRepository favBooksService =
                (FavouriteBooksService) getServletContext().getAttribute("favBooksService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        User user = null;
        List<Book> favBooks;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            Optional<List<Book>> favBooksPotential = favBooksService.getAllFav(user.getId());
            if (favBooksPotential.isPresent()) {
                favBooks = favBooksPotential.get();
                req.setAttribute("favBooks", favBooks);
            }
            req.getRequestDispatcher("/WEB-INF/templates/favBooks.ftl").forward(req, resp);

        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }
}
