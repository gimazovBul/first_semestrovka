package servlets.fav;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavBooksRepository;
import services.entityServices.UserService;
import services.favServices.FavouriteBooksService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "LikeBook", urlPatterns = "/addBookToFav")
public class AddBookToFavServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        FavBooksRepository favBooksService =
                (FavouriteBooksService)getServletContext().getAttribute("favBooksService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()) {
            User user = potentialUser.get();
            String bookId = req.getParameter("bookId");
            boolean state = favBooksService.isFav(user.getId(), Integer.valueOf(bookId));
            if (state) {
                state = false;
                favBooksService.deleteFromFav(user.getId(), Integer.valueOf(bookId));
            } else {
                state = true;
                favBooksService.saveToFav(user.getId(), Integer.valueOf(bookId));
            }
            ObjectMapper mapper = new ObjectMapper();
            resp.setContentType("text/json");
            resp.getWriter().write(mapper.writeValueAsString(state));
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
