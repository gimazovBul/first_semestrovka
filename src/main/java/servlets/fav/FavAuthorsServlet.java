package servlets.fav;

import models.Author;
import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavAuthorsRepository;
import services.entityServices.UserService;
import services.favServices.FavoriteAuthorsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "FavAuthors", urlPatterns = "/favAuthors")
public class FavAuthorsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        FavAuthorsRepository favAuthorsService =
                (FavoriteAuthorsService)getServletContext().getAttribute("favAuthorsService");
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        User user;
        Optional<List<Author>> potentialFavAuthors;
        List<Author> favAuthors;
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()){
            user = potentialUser.get();
            potentialFavAuthors = favAuthorsService.getAllFav(user.getId());
            if (potentialFavAuthors.isPresent()){
                favAuthors = potentialFavAuthors.get();
                req.setAttribute("favAuthors", favAuthors);
            }
            req.getRequestDispatcher("/WEB-INF/templates/favAuthors.ftl").forward(req, resp);

        }
        else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);


    }
}
