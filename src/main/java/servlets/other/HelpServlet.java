package servlets.other;

import models.User;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "Help", urlPatterns = "/help")
public class HelpServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        User user;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            req.setAttribute("user", user);
            req.setAttribute("admin", "lazyfucc69@gmail.com");
            req.getRequestDispatcher("/WEB-INF/templates/help.ftl").forward(req, resp);
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }


}
