package servlets.other;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Author;
import models.Book;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.BookRepository;
import services.entityServices.AuthorService;
import services.entityServices.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "AdvancedDoSearch", urlPatterns = "/doSearchAdv")
public class AdvanceDoSearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        BookRepository bookService = (BookService) getServletContext().getAttribute("bookService");
        AuthorRepository authorService = (AuthorService) getServletContext().getAttribute("authorService");
        String searchType = req.getParameter("type");
        String name = req.getParameter("name");
        String ratingMin = req.getParameter("ratingMin");
        String ratingMax = req.getParameter("ratingMax");
        resp.setContentType("text/json");
        if (searchType != null)
            switch (searchType) {
                case "books": {
                    if (ratingMin == null || ratingMax == null) {
                        if (name != null && !"".equals(name)) {
                            Optional<List<Book>> potentialBooks = bookService.findAllByName(name);
                            if (potentialBooks.isPresent()) {
                                resp.getWriter().write(mapper.writeValueAsString(potentialBooks.get()));
                            }
                        }
                    } else if (name == null || "".equals(name)) {
                        Optional<List<Book>> potentialBooks = bookService.findAllByRating(Integer.valueOf(ratingMin),
                                Integer.valueOf(ratingMax));
                        if (potentialBooks.isPresent())
                            resp.getWriter().write(mapper.writeValueAsString(potentialBooks.get()));
                    } else {
                        List<Book> booksByRating;
                        List<Book> booksByName;
                        Optional<List<Book>> potentialBooksByRating = bookService.findAllByRating(Integer.valueOf(ratingMin),
                                Integer.valueOf(ratingMax));
                        Optional<List<Book>> potentialBooksByName = bookService.findAllByName(name);
                        if (potentialBooksByRating.isPresent() && potentialBooksByName.isPresent()) {
                            booksByRating = potentialBooksByRating.get();
                            booksByName = potentialBooksByName.get();
                            booksByName.retainAll(booksByRating);
                            resp.getWriter().write(mapper.writeValueAsString(booksByName));

                        }
                    }
                    break;
                }
                case "authors": {
                    if (ratingMin == null || ratingMax == null) {
                        if (name != null && !"".equals(name)) {
                            Optional<Author> potentialAuthor = authorService.findByName(name);
                            if (potentialAuthor.isPresent()) {
                                resp.getWriter().write(mapper.writeValueAsString(potentialAuthor.get()));
                            }
                        }
                    } else if (name == null || "".equals(name)) {
                        Optional<List<Author>> potentialAuthors = authorService.findAllByRating(Integer.valueOf(ratingMin),
                                Integer.valueOf(ratingMax));
                        if (potentialAuthors.isPresent())
                            resp.getWriter().write(mapper.writeValueAsString(potentialAuthors.get()));
                    }
                    break;

                }
            }

    }
}