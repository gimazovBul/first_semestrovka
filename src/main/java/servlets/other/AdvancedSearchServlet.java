package servlets.other;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Author;
import models.Book;
import models.User;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.BookRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.AuthorService;
import services.entityServices.BookService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "AdvancedSearch", urlPatterns = "/searchAdv")
public class AdvancedSearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/templates/AdvancedSearch.ftl").forward(req, resp);
    }
}
