package servlets.other;

import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;
import services.other.FollowsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "profile", urlPatterns = {"/profile"})
public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FollowsRepository followService = (FollowsService) getServletContext().getAttribute("followService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        String otherUserId = req.getParameter("id");
        User user;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            if (otherUserId == null || Integer.valueOf(otherUserId) == session.getAttribute("user")) {
                req.setAttribute("user", user);
                req.getRequestDispatcher("/WEB-INF/templates/myProfile.ftl").forward(req, resp);
            } else {
                Optional<User> potentialOtherUser = userService.find(Integer.valueOf(otherUserId));
                User otherUser;
                if (potentialOtherUser.isPresent()) {
                    otherUser = potentialOtherUser.get();
                    req.setAttribute("user", otherUser);
                    String state = followService.findState(user.getId(), otherUser.getId());
                    switch (state) {
                        case "friends": {
                            req.setAttribute("friends", true);
                            break;
                        }
                        case "follows12": {
                            req.setAttribute("follows12", true);
                            break;
                        }
                        case "follows21": {
                            req.setAttribute("follows21", true);
                            break;
                        }
                    }
                }
                req.getRequestDispatcher("/WEB-INF/templates/otherProfile.ftl").forward(req, resp);
            }
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }
}
