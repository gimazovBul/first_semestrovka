package servlets.other;

import models.Author;
import models.Book;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.BookRepository;
import services.entityServices.AuthorService;
import services.entityServices.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "best5Authors", urlPatterns = "/bestAuthors")
public class Best5AuthorsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AuthorRepository authorService = (AuthorService)getServletContext().getAttribute("authorService");
        Optional<List<Author>> potentialAuthor = authorService.findBest5Authors();
        potentialAuthor.ifPresent(authors -> req.setAttribute("authors", authors));
    }
}
