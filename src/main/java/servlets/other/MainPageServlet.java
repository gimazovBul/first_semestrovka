package servlets.other;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "mainPage", urlPatterns = "/main")
public class MainPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/bestBooks").include(req, resp);
        req.getRequestDispatcher("/bestAuthors").include(req, resp);
        req.getRequestDispatcher("/WEB-INF/templates/mainPage.ftl").forward(req, resp);
    }
}
