package servlets.other;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Author;
import models.Book;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.BookRepository;
import services.entityServices.AuthorService;
import services.entityServices.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "SimpleAjaxSearch", urlPatterns = "/search")
public class SimpleSearchAjaxServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookRepository bookService = (BookService) getServletContext().getAttribute("bookService");
        AuthorRepository authorService = (AuthorService) getServletContext().getAttribute("authorService");
        String query = req.getParameter("query");
        List<Object> result = new ArrayList<>();
        Optional<List<Book>> books = bookService.findByLikePattern(query);
        Optional<List<Author>> authors = authorService.findByLikePattern(query);
        books.ifPresent(result::addAll);
        authors.ifPresent(result::addAll);
        ObjectMapper mapper = new ObjectMapper();
        String resultString = mapper.writeValueAsString(result);
        resp.setContentType("text/json");
        resp.getWriter().write(resultString);
    }
}
