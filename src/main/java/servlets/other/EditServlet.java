package servlets.other;

import models.User;
import org.mindrot.jbcrypt.BCrypt;
import repositories.entittyRepositories.CountryRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.CountryService;
import services.entityServices.UserService;
import services.other.PartReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Edit", urlPatterns = "/edit")
@MultipartConfig
public class EditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        HttpSession session = req.getSession(false);
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()) {
            req.setAttribute("user", potentialUser.get());
            req.setAttribute("password", potentialUser.get().getPassword());
            req.getRequestDispatcher("/WEB-INF/templates/edit.ftl").forward(req, resp);

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        CountryRepository countryService = (CountryService) getServletContext().getAttribute("countryService");
        HttpSession session = req.getSession(false);
        List<String> errMessages = new ArrayList<>();
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        HttpServletRequest httpServletRequest = new HttpServletRequestWrapper(req);
        if (potentialUser.isPresent()) {
            User user = potentialUser.get();

            PartReader partReader = new PartReader();
            String email = partReader.read(httpServletRequest.getPart("email"));
            String nickname = partReader.read(httpServletRequest.getPart("nickname"));
            String name = partReader.read(httpServletRequest.getPart("name"));
            Integer countryId = Integer.valueOf(partReader.read(httpServletRequest.getPart("countryId")));
            String deleted = partReader.read(httpServletRequest.getPart("del"));
            Part part = req.getPart("avatar");

            String partName;
            String[] imageNameArr;
            imageNameArr = Paths.get(part.getSubmittedFileName()).getFileName().toString().split("\\.");
            String filename = Math.random() + "." + imageNameArr[imageNameArr.length - 1];
            String localdir = "uploads";
            String pathDir = getServletContext().getRealPath("") + File.separator + localdir;
            File dir = new File(pathDir);
            if (!dir.exists()) {
                dir.mkdir();
            }
            String fullpath = pathDir + File.separator + filename;
            part.write(fullpath);
            partName = "/" + localdir + "/" + filename;
            user.setAvatar(partName);
            if (email != null && !"".equals(email)) {
                if (userService.findByEmail(email).isPresent() && !user.getEmail().equals(email)) {
                    errMessages.add("User with this email already exists");
                } else user.setEmail(email);
            }
            if (nickname != null && !"".equals(nickname)) {
                if (userService.findByNickname(nickname).isPresent() && !user.getNickname().equals(nickname)) {
                    errMessages.add("User with this nickname already exists");
                } else user.setNickname(nickname);
            }
            user.setCountry(countryService.findById(countryId).get());
            if (name != null && !"".equals(name)) {
                user.setName(name);
            }
            System.out.println(user);

            if (Boolean.parseBoolean(deleted)) {
                user.setDeleted(true);
                req.getRequestDispatcher("/logout").include(req, resp);
            }
            if (errMessages.size() != 0) {
                req.setAttribute("errMessages", errMessages);
                req.getRequestDispatcher("/WEB-INF/templates/edit.ftl").forward(req, resp);
            } else {
                System.out.println("updating");
                userService.update(user);
                resp.sendRedirect("/profile");
            }
        }
    }

}
