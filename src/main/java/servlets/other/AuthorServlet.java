package servlets.other;

import models.Author;
import models.User;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavAuthorsRepository;
import services.entityServices.AuthorService;
import services.entityServices.UserService;
import services.favServices.FavoriteAuthorsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Author", urlPatterns = "/author")
public class AuthorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        AuthorRepository authorService = (AuthorService) getServletContext().getAttribute("authorService");
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FavAuthorsRepository favAuthorsService =
                (FavoriteAuthorsService) getServletContext().getAttribute("favAuthorsService");
        Boolean fav;
        User user = null;
        Author author = null;
        String authorIdRequest = req.getParameter("authorId");
        if (authorIdRequest == null) {
            Optional<List<Author>> potentialAuthors = authorService.findAll();
            if (potentialAuthors.isPresent()) {
                req.setAttribute("authors", potentialAuthors.get());
                req.getRequestDispatcher("/WEB-INF/templates/authors.ftl").forward(req, resp);
            }
        } else {
            if (session != null) {
                Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
                if (potentialUser.isPresent()) {
                    user = potentialUser.get();
                    Optional<Author> potentialAuthor = authorService.find(Integer.valueOf(authorIdRequest));
                    if (potentialAuthor.isPresent()) {
                        author = potentialAuthor.get();
                        fav = favAuthorsService.isFav(user.getId(), author.getId());
                        req.setAttribute("fav", fav);
                    }
                }
            } else {
                Optional<Author> potentialAuthor = authorService.find(Integer.valueOf(authorIdRequest));
                if (potentialAuthor.isPresent()) {
                    author = potentialAuthor.get();
                }
            }
            if (author == null) req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
            if (user != null) req.setAttribute("user", user);
            req.setAttribute("author", author);
            req.getRequestDispatcher("/WEB-INF/templates/author.ftl").forward(req, resp);

        }
    }
}
