package servlets.other;

import models.User;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Users", urlPatterns = "/users")
public class UsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        Optional<List<User>> potentialUsers = userService.findAll();
        if (potentialUsers.isPresent()) {
            req.setAttribute("users", potentialUsers.get());
            req.getRequestDispatcher("/WEB-INF/templates/users.ftl").forward(req, resp);
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }
}
