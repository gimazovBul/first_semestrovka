package servlets.other;

import models.Book;
import repositories.entittyRepositories.BookRepository;
import services.entityServices.BookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Best5Books", urlPatterns = "/bestBooks")
public class Best5BooksServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookRepository bookService = (BookService)getServletContext().getAttribute("bookService");
        Optional<List<Book>> potentialBooks = bookService.findBest5();
        if (potentialBooks.isPresent()) {
            req.setAttribute("books", potentialBooks.get());
        }
        else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
