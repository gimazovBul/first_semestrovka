package servlets.other;

import models.Book;
import models.User;
import repositories.entittyRepositories.BookRepository;
import repositories.entittyRepositories.UserRepository;
import repositories.favRepositories.FavBooksRepository;
import services.entityServices.BookService;
import services.entityServices.UserService;
import services.favServices.FavouriteBooksService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "TestingBook", urlPatterns = "/book")
public class BookServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        BookRepository bookService = (BookService) getServletContext().getAttribute("bookService");
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FavBooksRepository favBooksService =
                (FavouriteBooksService) getServletContext().getAttribute("favBooksService");
        Boolean fav;
        User user = null;
        Book book = null;
        String bookIdRequest = req.getParameter("bookId");
        if (bookIdRequest == null) {
            Optional<List<Book>> potentialBooks = bookService.findAll();
            if (potentialBooks.isPresent()) {
                req.setAttribute("books", potentialBooks.get());
                req.getRequestDispatcher("/WEB-INF/templates/books.ftl").forward(req, resp);
            }
        } else {
            if (session != null) {
                Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
                if (potentialUser.isPresent()) {
                    user = potentialUser.get();
                    Optional<Book> potentialBook = bookService.find(Integer.valueOf(bookIdRequest));
                    if (potentialBook.isPresent()) {
                        book = potentialBook.get();
                        fav = favBooksService.isFav(user.getId(), book.getId());
                        req.setAttribute("fav", fav);
                    }
                }
            } else {
                Optional<Book> potentialBook = bookService.find(Integer.valueOf(bookIdRequest));
                if (potentialBook.isPresent()) {
                    book = potentialBook.get();
                }
            }
            if (book == null) req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
            req.setAttribute("book", book);
            if (user != null) req.setAttribute("user", user);
            req.getRequestDispatcher("/WEB-INF/templates/book.ftl").forward(req, resp);

        }
    }
}
