package servlets.other;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.User;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "userSearch", urlPatterns = "/searchUser")
public class UserSearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        ObjectMapper mapper = new ObjectMapper();
        resp.setContentType("text/json");
        String nickname = req.getParameter("nickname");
        Optional<User> potentialUser = userService.findByNickname(nickname);
        if (potentialUser.isPresent()) resp.getWriter().write(mapper.writeValueAsString(potentialUser.get()));
    }
}
