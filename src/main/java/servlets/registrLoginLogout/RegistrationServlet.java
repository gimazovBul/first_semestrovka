package servlets.registrLoginLogout;

import com.dexafree.simplesec.SHA256;
import models.User;
import org.mindrot.jbcrypt.BCrypt;
import repositories.cookieRepositories.CookieRepository;
import repositories.entittyRepositories.CountryRepository;
import repositories.entittyRepositories.UserRepository;
import security.AuthToken;
import services.cookieService.CookieService;
import services.entityServices.CountryService;
import services.entityServices.UserService;
import services.other.PartReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.net.URL;
import java.nio.file.Paths;
import java.time.Year;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@MultipartConfig
@WebServlet(name = "Registration", urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            Integer id = (Integer) session.getAttribute("user");
            if (id != null) {
                resp.sendRedirect("/profile");
            }
        } else {
            req.getRequestDispatcher("/WEB-INF/templates/registration.ftl").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        CookieRepository cookieService = (CookieService) getServletContext().getAttribute("cookieService");
        CountryRepository countryService = (CountryService) getServletContext().getAttribute("countryService");
        HttpServletRequest httpServletRequest = new HttpServletRequestWrapper(req);
        PartReader partReader = new PartReader();
        String email = partReader.read(httpServletRequest.getPart("email"));
        String password = partReader.read(httpServletRequest.getPart("confirmPassw"));
        String nickname = partReader.read(httpServletRequest.getPart("nickname"));
        String name = partReader.read(httpServletRequest.getPart("name"));
        Integer age = Integer.valueOf(partReader.read(httpServletRequest.getPart("age")));
        Integer countryId = Integer.valueOf(partReader.read(httpServletRequest.getPart("countryId")));
        String rememberMe = partReader.read(httpServletRequest.getPart("rememberMe"));
        Part part = req.getPart("avatar");

        String partName;
        String[] imageNameArr;
        imageNameArr = Paths.get(part.getSubmittedFileName()).getFileName().toString().split("\\.");
        String filename = Math.random() + "." + imageNameArr[imageNameArr.length - 1];
        String localdir = "uploads";
        String pathDir = getServletContext().getRealPath("") + File.separator + localdir;
        File dir = new File(pathDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String fullpath = pathDir + File.separator + filename;
        part.write(fullpath);
        partName = "/" + localdir + "/" + filename;

        String errMsg;
        if (userService.findByEmail(email).isPresent() || userService.findByNickname(nickname).isPresent()) {
            errMsg = "User with that email or nickname already exists";
            req.setAttribute("errMsg", errMsg);
            req.getRequestDispatcher("/WEB-INF/templates/registration.ftl").forward(req, resp);
        } else {
            Boolean adult = age < Year.now().getValue() - 18;
            User user = new User(nickname,
                    email, countryService.findById(countryId).get(), adult, false,
                    partName, name, BCrypt.hashpw(password, BCrypt.gensalt()));
            userService.save(user);
            HttpSession session = req.getSession();
            if (Boolean.parseBoolean(rememberMe)) {
                Cookie selector = new Cookie("selector", randomAlphanumeric(12));
                selector.setMaxAge(60 * 60 * 24 * 365);//one year
                Cookie validator = new Cookie("validator", randomAlphanumeric(56));
                validator.setMaxAge(60 * 60 * 24 * 365);
                resp.addCookie(selector);
                resp.addCookie(validator);
                AuthToken authToken = new AuthToken(user.getId(), selector.getValue(), SHA256.hash(validator.getValue()));
                cookieService.save(authToken);
            }
            session.setAttribute("user", user.getId());
            resp.sendRedirect("/profile");

        }
    }
}
