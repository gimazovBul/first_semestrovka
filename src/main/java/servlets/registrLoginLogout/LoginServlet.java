package servlets.registrLoginLogout;


import com.dexafree.simplesec.SHA256;
import models.User;
import repositories.cookieRepositories.CookieRepository;
import repositories.entittyRepositories.UserRepository;
import security.AuthToken;
import services.cookieService.CookieService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@WebServlet(name = "Login", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            Integer id = (Integer) session.getAttribute("user");
            if (id != null) {
                resp.sendRedirect("/profile");
            }
        } else {
            req.getRequestDispatcher("/WEB-INF/templates/loginFormPage.ftl").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        CookieRepository cookieService = (CookieService) getServletContext().getAttribute("cookieService");
        User user;
        String email = req.getParameter("email");
        String nickname = req.getParameter("nickname");
        String password = req.getParameter("password");
        String rememberMe = req.getParameter("rememberMe");
        Optional<User> potentialUser = null;
        if (email != null && !"".equals(email))
            potentialUser = userService.checkEmailPassword(email, password);
        else potentialUser = userService.checkNicknamePassword(nickname, password);
        if (potentialUser.isPresent()) {
            HttpSession session = req.getSession();
            user = potentialUser.get();
            if (Boolean.parseBoolean(rememberMe)) {
                Cookie selector = new Cookie("selector", randomAlphanumeric(12));
                selector.setMaxAge(60 * 60 * 24 * 365);//one year
                Cookie validator = new Cookie("validator", randomAlphanumeric(56));
                validator.setMaxAge(60 * 60 * 24 * 365);
                resp.addCookie(selector);
                resp.addCookie(validator);
                AuthToken authToken = new AuthToken(user.getId(), selector.getValue(), SHA256.hash(validator.getValue()));
                cookieService.save(authToken);
            }
            session.setAttribute("user", user.getId());
            resp.sendRedirect("/profile");
        } else {
            req.setAttribute("err", "Wrong email/nickname or password");
            req.getRequestDispatcher("/WEB-INF/templates/loginFormPage.ftl").forward(req, resp);
        }
    }
}
