package servlets.registrLoginLogout;

import repositories.cookieRepositories.CookieRepository;
import security.AuthToken;
import services.cookieService.CookieService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "logout", urlPatterns = {"/logout"})
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        CookieRepository cookieService = (CookieService) getServletContext().getAttribute("cookieService");

        if (session == null) resp.sendRedirect("/login");
        else {
            Cookie[] cookies = req.getCookies();
            String selectorCookie = null;
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("selector")) {
                    selectorCookie = cookie.getValue();
                    break;
                }
            }
            if (selectorCookie != null) {
                Optional<AuthToken> potentialToken = cookieService.getToken(selectorCookie);
                potentialToken.ifPresent(authToken -> cookieService.delete(authToken.getId()));
                Cookie selector = new Cookie("selector", null);
                selector.setMaxAge(0);
                Cookie validator = new Cookie("validator", null);
                validator.setMaxAge(0);
                resp.addCookie(selector);
                resp.addCookie(validator);
            }
            session.setAttribute("user", null);
            session.invalidate();
            resp.sendRedirect("/login");
        }

    }
}
