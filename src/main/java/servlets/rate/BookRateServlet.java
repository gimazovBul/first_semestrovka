package servlets.rate;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Book;
import models.User;
import repositories.entittyRepositories.BookRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.BookService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.Optional;

@WebServlet(name = "bookRate", urlPatterns = "/rateBook")
public class BookRateServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        Integer userId = (Integer) session.getAttribute("user");
        String bookId = req.getParameter("bookId");
        String rate = req.getParameter("rate");
        ObjectMapper mapper = new ObjectMapper();
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        BookRepository bookService = (BookService) getServletContext().getAttribute("bookService");
        User user;
        Book book;
        Optional<User> potentialUser = userService.find(userId);
        Optional<Book> potentialBook = bookService.find(Integer.valueOf(bookId));
        if (potentialUser.isPresent()){
            user = potentialUser.get();
            if (potentialBook.isPresent()){
                book = potentialBook.get();
                bookService.rate(user.getId(), book.getId(), Integer.parseInt(rate));
                resp.setContentType("text/json");
                String result = mapper.writeValueAsString(bookService.find(Integer.valueOf(bookId)).get());
                resp.getWriter().write(result);
            }
        }
    }
}
