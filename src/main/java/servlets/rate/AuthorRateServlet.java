package servlets.rate;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.Author;
import models.User;
import repositories.entittyRepositories.AuthorRepository;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.AuthorService;
import services.entityServices.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "AuthorRate", urlPatterns = "/rateAuthor")
public class AuthorRateServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("working");
        HttpSession session = req.getSession(false);
        Integer userId = (Integer) session.getAttribute("user");
        String authorId = req.getParameter("authorId");
        String rate = req.getParameter("rate");
        ObjectMapper mapper = new ObjectMapper();
        User user;
        Author author;
        AuthorRepository authorService = (AuthorService) getServletContext().getAttribute("authorService");
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        Optional<User> potentialUser = userService.find(userId);
        Optional<Author> potentialAuthor = authorService.find(Integer.valueOf(authorId));
        if (potentialUser.isPresent()){
            user = potentialUser.get();
            if (potentialAuthor.isPresent()){
                author = potentialAuthor.get();
                authorService.rate(user.getId(), author.getId(), Integer.parseInt(rate));
                resp.setContentType("text/json");
                String result = mapper.writeValueAsString(authorService.find(Integer.valueOf(authorId)).get());
                System.out.println(result);
                resp.getWriter().write(result);
            }
        }
    }
}
