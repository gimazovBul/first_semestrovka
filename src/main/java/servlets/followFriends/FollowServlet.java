package servlets.followFriends;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;
import services.other.FollowsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.Optional;

@WebServlet(name = "Follow", urlPatterns = "/follow")
public class FollowServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FollowsRepository followService = (FollowsService) getServletContext().getAttribute("followService");
        HttpSession session = req.getSession(false);
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        Optional<User> potentialOtherUser = userService.find(Integer.valueOf(req.getParameter("id")));
        User userToFollow;
        ObjectMapper mapper = new ObjectMapper();
        if (potentialUser.isPresent()) {
            User user = potentialUser.get();
            if (potentialOtherUser.isPresent()) {
                userToFollow = potentialOtherUser.get();
                String userState = followService.findState(user.getId(), userToFollow.getId());
                if (userState.equals("friends") || userState.equals("follows12")) {
                    followService.unfollow(user.getId(), userToFollow.getId());
                    if (userState.equals("friends")) userState = "follows21";
                    else userState = "none";
                } else {
                    followService.follow(user.getId(), userToFollow.getId());
                    if (userState.equals("follows21")) userState = "friends";
                    else userState = "follows12";
                }
                resp.setContentType("text/json");
                resp.getWriter().write(mapper.writeValueAsString(userState));
            }

        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
