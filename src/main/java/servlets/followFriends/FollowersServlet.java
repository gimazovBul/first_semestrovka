package servlets.followFriends;

import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;
import services.other.FollowsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Followers", urlPatterns = "/followers")
public class FollowersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        FollowsRepository followService = (FollowsService) getServletContext().getAttribute("followService");
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        Optional<List<User>> potentialFollowers;
        List<User> followers = new ArrayList<>();
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        if (potentialUser.isPresent()) {
            potentialFollowers = followService.findFollowers(potentialUser.get().getId());
            if (potentialFollowers.isPresent()) followers = potentialFollowers.get();
            req.setAttribute("followers", followers);
            req.getRequestDispatcher("/WEB-INF/templates/followers.ftl").forward(req, resp);
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }
}
