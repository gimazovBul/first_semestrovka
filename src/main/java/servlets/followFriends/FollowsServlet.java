package servlets.followFriends;

import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;
import services.other.FollowsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Follows", urlPatterns = "/follows")
public class FollowsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService)getServletContext().getAttribute("userService");
        FollowsRepository followService = (FollowsService) getServletContext().getAttribute("followService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        List<User> follows = new ArrayList<>();
        if (potentialUser.isPresent()) {
            User user = potentialUser.get();
            Optional<List<User>> potentialFollows = followService.findFollows(user.getId());
            if (potentialFollows.isPresent()) {
                follows = potentialFollows.get();
            }
            req.setAttribute("follows", follows);
            req.getRequestDispatcher("/WEB-INF/templates/follows.ftl").forward(req, resp);
        } else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);
    }
}
