package servlets.followFriends;

import models.User;
import repositories.entittyRepositories.UserRepository;
import repositories.other.FollowsRepository;
import services.entityServices.UserService;
import services.other.FollowsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "Friends", urlPatterns = "/friends")
public class FriendsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        UserRepository userService = (UserService) getServletContext().getAttribute("userService");
        FollowsRepository followService = (FollowsService) getServletContext().getAttribute("followService");
        Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
        User user;
        List<User> friends;
        if (potentialUser.isPresent()) {
            user = potentialUser.get();
            Optional<List<User>> potentialFriends = followService.findFriends(user.getId());
            if (potentialFriends.isPresent()){
                friends = potentialFriends.get();
                req.setAttribute("friends", friends);
            }
            req.getRequestDispatcher("/WEB-INF/templates/friends.ftl").forward(req, resp);

        }
        else req.getRequestDispatcher("/WEB-INF/templates/error.ftl").forward(req, resp);

    }

}
