package security.filters;

import models.User;
import repositories.entittyRepositories.UserRepository;
import services.entityServices.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

@WebFilter(urlPatterns = {"/profile/*", "/messages/*",
        "/favAuthors", "/favBooks", "/followers",
        "/follows", "/edit", "/help", "/friends"})
public class DeletedFilter extends HttpFilter {

    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "olisef67"; //Захардкодил, надо бы по-другому

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        try {
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            UserRepository userService = new UserService(connection);
            Optional<User> potentialUser = userService.find((Integer) session.getAttribute("user"));
            if (potentialUser.isPresent()){
                if (potentialUser.get().getDeleted()){
                    response.sendRedirect("/undel");
                }
                else chain.doFilter(req, res);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
