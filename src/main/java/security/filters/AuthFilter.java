package security.filters;


import com.dexafree.simplesec.SHA256;
import org.apache.commons.lang3.RandomStringUtils;
import repositories.cookieRepositories.CookieRepository;
import security.AuthToken;
import services.cookieService.CookieService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;

@WebFilter(filterName = "AuthFilter", urlPatterns = {"/profile/*", "/messages/*",
        "/favAuthors", "/favBooks", "/followers",
        "/follows", "/edit", "/help", "/friends"})
public class AuthFilter implements Filter {

    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "olisef67"; //Захардкодил, надо бы по-другому

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("user") != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            Cookie[] cookies = request.getCookies();
            String selector = null;
            String validator = null;
            CookieRepository cookieService;
            Optional<AuthToken> potentialHashedAuthToken;
            AuthToken token;
            try {
                Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                cookieService = new CookieService(connection);
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("selector")) {
                    selector = cookie.getValue();
                } else if (cookie.getName().equals("validator")) {
                    validator = cookie.getValue();
                }
            }
            if (selector != null && validator != null) {
                potentialHashedAuthToken = cookieService.getToken(selector);
                if (potentialHashedAuthToken.isPresent()) {
                    token = potentialHashedAuthToken.get();
                    if (SHA256.hash(validator).equals(token.getValidator())) {
                        session = request.getSession();
                        session.setAttribute("user", token.getUserId());
                        String selectorNew = RandomStringUtils.randomAlphanumeric(12);
                        String validatorNew = RandomStringUtils.randomAlphanumeric(56);
                        Cookie selectorNewCookie = new Cookie("selector", selectorNew);
                        selectorNewCookie.setMaxAge(60 * 60 * 24 * 365);
                        Cookie validatorNewCookie = new Cookie("validator", validatorNew);
                        validatorNewCookie.setMaxAge(60 * 60 * 24 * 365);
                        response.addCookie(selectorNewCookie);
                        response.addCookie(validatorNewCookie);
                        token.setSelector(selectorNew);
                        token.setValidator(validatorNew);
                        cookieService.update(token);
                        request.getRequestDispatcher("/profile").forward(request, response);
                        filterChain.doFilter(servletRequest, servletResponse);
                    }
                }
            }
        }
         if (session == null) response.sendRedirect("/login");
    }
}
