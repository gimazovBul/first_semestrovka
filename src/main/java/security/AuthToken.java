package security;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class AuthToken {
    private Integer id;
    private Integer userId;
    private String selector;
    private String validator;

    public AuthToken(Integer userId, String selector, String validator) {
        this.userId = userId;
        this.selector = selector;
        this.validator = validator;
    }
}
