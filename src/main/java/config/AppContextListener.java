package config;

import repositories.cookieRepositories.CookieRepository;
import repositories.entittyRepositories.*;
import repositories.favRepositories.FavAuthorsRepository;
import repositories.favRepositories.FavBooksRepository;
import repositories.other.FollowsRepository;
import services.cookieService.CookieService;
import services.entityServices.*;
import services.favServices.FavoriteAuthorsService;
import services.favServices.FavouriteBooksService;
import services.other.FollowsService;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.SQLException;


@WebListener
public class AppContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext ctx = sce.getServletContext();
        String username = ctx.getInitParameter("username");
        String url = ctx.getInitParameter("url");
        String password = ctx.getInitParameter("password");
        String limit = ctx.getInitParameter("limit");
        DbConnectionManager connectionManager;
        ctx.setAttribute("limit", limit);
        try {
            connectionManager = new DbConnectionManager(username, password, url);
            ctx.setAttribute("DbConnection", connectionManager.getConnection());
        } catch (SQLException | ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
        Connection connection = connectionManager.getConnection();
        CookieRepository cookieService = new CookieService(connection);
        AuthorRepository authorService = new AuthorService(connection);
        BookRepository bookService = new BookService(connection);
        CountryRepository countryService = new CountryService(connection);
        LanguageRepository languageService = new LanguageService(connection);
        MessageRepository messageService = new MessageService(connection);
        SubjectRepository subjectService = new SubjectService(connection);
        UserRepository userService = new UserService(connection);
        FavAuthorsRepository favAuthorsService = new FavoriteAuthorsService(connection);
        FavBooksRepository favBooksService = new FavouriteBooksService(connection);
        FollowsRepository followsService = new FollowsService(connection);
        ctx.setAttribute("cookieService", cookieService);
        ctx.setAttribute("authorService", authorService);
        ctx.setAttribute("bookService", bookService);
        ctx.setAttribute("countryService", countryService);
        ctx.setAttribute("languageService", languageService);
        ctx.setAttribute("messageService", messageService);
        ctx.setAttribute("subjectService", subjectService);
        ctx.setAttribute("userService", userService);
        ctx.setAttribute("favAuthorsService", favAuthorsService);
        ctx.setAttribute("favBooksService", favBooksService);
        ctx.setAttribute("followService", followsService);
    }


    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Connection connection = (Connection) sce.getServletContext().getAttribute("DbConnection");
        try {
            connection.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
