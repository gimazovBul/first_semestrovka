<!doctype html>
<html lang="en">
<head>
    <script type="application/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300" type="text/css">
    <link rel="stylesheet" href="../../public/pages/css/styleProfile.css" type="text/css">


    <script type="application/javascript">
        function f() {
            var input = $('#id');
            $.ajax({
                url:  "/follow",
                data: {"id": input.val()},
                type: 'post',
                dataType: "json",
                success: function (resp) {
                    if (resp === 'friends') {
                        $('#text').html("friends");
                        $('#submitInp').val("unfriend");
                    }
                    else if (resp === 'follows12'){
                        $('#text').html("you follow");
                        $('#submitInp').val("unfollow");
                    }

                    else if (resp === 'follows21'){
                        $('#text').html("your follower");
                        $('#submitInp').val("berfriend");
                    }

                    else{
                        $('#text').html("follow");
                        $('#submitInp').val("follow");
                    }
                }
            })
        }
    </script>
    <title>Hello, world!</title>
</head>
<body>


<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <#if friends?? || follows12?? || follows21??>
        <#if friends?? && friends == true>
            <#assign text = 'You are friends'>
            <#assign submitValue = 'unfriend'>
        <#elseif follows12?? && follows12 == true>
            <#assign text = 'you follows'>
            <#assign submitValue = 'unfollow'>
        <#else>
            <#assign text = 'your follower'>
            <#assign submitValue = 'befriend'>
        </#if>
    <#else>
        <#assign text = 'follow'>
        <#assign submitValue = 'follow'>
    </#if>

    <div class="row">
        <div class="col-md-6 img">
            <img width="250" height="250" src="${user.avatar}"
                 alt="" class="img-rounded">
        </div>
        <div class="col-md-6 details">
            <blockquote>
                <h5>${user.nickname}</h5>

            </blockquote>
            <p>
                ${user.name} <br>
                ${user.country.name}<br>
                <div id="text" style="color: #64038f; font-size: larger">${text}</div>
            </p>
        </div>
    </div>
    <br>

    <div class="row second">
        <form action="/follow" method="post" onsubmit="f(); return false;">
            <input type="hidden" name="id" id="id" value="${user.id}">
            <input type="submit" id="submitInp" class="btn btn-light" value="${submitValue}">
        </form>
    </div>

</div>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>