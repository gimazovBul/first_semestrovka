<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300" type="text/css">
    <link rel="stylesheet" href="../../public/pages/css/styleFav.css" type="text/css">

    <title>Hello, world!</title>
    <script type="application/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="application/javascript">
        function search() {
            var nickname = $('#nickname');
            var nickname2 = $('#nickname2');
            var avatar = $('#avatar');
            var userId = $('#userId');
            console.log(nickname.length);
            if (nickname.val().length > 0) {
                $.ajax({
                    url: "/searchUser",
                    data: {"nickname": nickname.val()},
                    type: "get",
                    dataType: "json",
                    success: function (resp) {
                        if (resp !== null) {
                            $('#users').attr("hidden", true);
                            $('#res').attr("hidden", false);
                            avatar.attr("src", resp.avatar);
                            nickname2.html(resp.nickname);
                            userId.attr("href", "/profile?id=" + resp.id);
                        }

                    }
                });
            } else {
                $('#users').attr("hidden", false);
                $('#res').attr("hidden", true);

            }
        }
    </script>
</head>
<body>


<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <form method="get" action="/searchUser" oninput="search(); return false;" onsubmit="search();return false;">
        <input name="nickname" placeholder="nickname" id="nickname">
    </form>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>


<section class="l-page">

    <div class="efg" style="background-color: #ffffff;">

        <div class="nn6">
            <div class="form-row">
                <div class="form-group col-md-12">

                    <h2>Users</h2>


                    <#if users??>
                    <div id="users">
                        <#list users as user>

                            <div class="kniga">
                                <div class="card-group" style="background-color: rgba(197,197,197,0.16);">
                                    <#if user.avatar??>
                                        <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                            <img src="${user.avatar}" class="card-img" width="120"
                                                 height="140">
                                        </div>
                                    </#if>
                                    <div class="form-group col-md-11" style="background-color: rgba(197,197,197,0.0);">
                                        <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                            <a href="/profile?id=${user.id}"><h4
                                                        class="text"
                                                        id="text">${user.nickname}</h4>
                                            </a>

                                            &nbsp;

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </#list>
                        <div class="kniga">

                            </#if>

                        </div>
                    </div>
                    <div id="res" hidden class="card-group" style="background-color: rgba(197,197,197,0.16);">
                        <div class="card">
                            <img id="avatar" src="" class="card-img" width="120"
                                 height="140">
                        </div>
                        <div class="form-group col-md-11" style="background-color: rgba(197,197,197,0.0);">
                            <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                <a id="userId" href=""><h4 id="nickname2"
                                                           class="text"></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>