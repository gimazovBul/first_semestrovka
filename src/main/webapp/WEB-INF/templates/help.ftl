<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../public/pages/css/style3.css">

    <title>Hello, world!</title>

    <script type="application/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script type="application/javascript">
        function sendMess() {
            var userTO = $('#userTo');
            var text = $('#helpText');
            $.ajax({
                url: "/sendHelp",
                data: {
                    "adminEmail": userTO.val(), "helpText": text.val()
                },
                type: 'post',
                dataType: "json",
                success: function (resp) {
                    alert(resp);
                }
            })
        }
    </script>

</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<form action="/help" method="post" onsubmit="sendMess(); return false;">
    <div class="form-group">

        <div class="nn4">
            <div class="vniz">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="card-group">
                            <div class="card" style="background: rgba(47, 92, 117, 0.0);">
                                <h5>To:</h5>
                            </div>
                            <div class="form-group col-md-11">
                                <div class="card">
                                    <input type="text" name="adminEmail" id="userTo" disabled value="${admin}">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <textarea class="form-control" id="helpText" rows="20" cols="100"
                  name="comment"></textarea>
    </div>
    <input type="submit" class="btn btn-light" value="send">
</form>
</body>
</html>