<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300" type="text/css">
    <link rel="stylesheet" href="../../public/pages/css/style3.css" type="text/css">

    <script type="application/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="application/javascript">
        function fav() {
            var input = $('#bookId');
            $.ajax({
                url: "/addBookToFav",
                data: {"bookId": input.val()},
                type: "post",
                dataType: "json",
                success: function (resp) {
                    if (resp === true) {
                        $('#submitInp').val("dislike");
                        $('#text').html("Remove from favorite");
                    } else {
                        $('#submitInp').val("like");
                        $('#text').html("Add to favorite");
                    }
                }
            })
        }
    </script>
    <script type="application/javascript">
        function toRate() {
            var toRate = $('#toRateSel');
            var currRate = $('#currRate');
            var input = $('#bookId2');
            var rateCount = $('#rateCount');
            $.ajax({
                url: "/rateBook",
                data: {"bookId": input.val(), "rate": toRate.val()},
                type: "post",
                dataType: "json",
                success: function (resp) {
                    if (resp !== null) {
                        currRate.html(resp.rating);
                        rateCount.html("Rates amount: " + resp.ratingCount);
                    }
                }
            })
        }
    </script>
    <title>Hello, world!</title>
</head>
<body>



<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>

<section class="l-page">

    <div class="efg" style="background-color: #ffffff;">


        <!--    <div class="card" style="width: 18rem;">
                <img src="image/hes.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                </div>

            </div>
    -->
        <#if fav??>
            <#if fav == true>
                <#assign submitValue = 'dislike'>
                <#assign text = 'Remove from favorite'>
            <#else>
                <#assign submitValue = 'like'>
                <#assign text = 'Add to favorites'>
            </#if>
        </#if>

        <div class="fr">
            <div class="form-row">
                <div class="form-group col-md-12">

                    <div class="card-group">
                        <div class="card">
                            <div class="col-md-10">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="${book.image}" width="400" height="500" class="card-img" alt=>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>


                        </div>

                        <div class="card">
                            <h1 class="card-title">${book.name}</h1>
                            <a href="/author?authorId=${book.author.id}"><h3 class="card-title">${book.author.name}</h3></a>
                            <p class="card-text">
                                <l class="text-muted"> Language: ${book.lang.name}</l>
                            </p>
                            <p class="card-text">
                                <l class="text-muted"> Publishing year: ${book.year}</l>
                            </p>
                            <p class="card-text">
                                <l class="text-muted"> Subject: ${book.subject.name}</l>
                            </p>
                            <p class="card-text"><small class="text-muted">${book.about}</small></p>


                        </div>
                        <div class="card" style="margin-top: -100px">

                            <div class="col-md-3">
                                <div class="abc" style="background-color: #ffffff;">
                                        <#if fav?? && user.deleted == false>
                                            <div style="display: inline-block">Rate</div>
                                            <form id="rateBookForm" method="post" action="/rateBook"
                                                  style="margin-top: -85px; margin-left: -20px; display: inline-block"
                                                  onsubmit="toRate(); return false;">
                                                <input type="hidden" name="bookId" id="bookId2" value="${book.id}">
                                                <select id="toRateSel">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                <input type="submit" class="btn btn-light" value="rate">
                                            </form>
                                        </#if>


                                    <div class="oma" style="margin-top: 50px">
                                        <div class="nestar">
                                            <div class="row no-gutters">
                                                <p class="card-text">
                                                    <l class="text-muted" id="currRate">${book.rating}</l>
                                                </p>
                                                <img src="../../public/pages/image/kisss.png" class="card-img2">
                                                <p class="card-text">
                                                    <l class="text-muted" id="rateCount"> Rates amount: ${book.ratingCount}</l>
                                                </p>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="nnn">
                                       <form method="post" action="/loadPdf">
                                           <input type="hidden" name="bookId" value="${book.id}">
                                           <input type="submit" style="height: 40px; width: 150px; margin-top: 10px" class="btn btn-light" value="Read">
                                       </form>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>
        </div>
        <#if fav?? && user.deleted == false>
            <div id="text">${text}</div><br>
            <form action="/addBookToFav" method="post" id="likeBookForm" onsubmit="fav(); return false;">
                <input type="hidden" name="bookId" id="bookId" value="${book.id}">
            </form>
            <input type="submit" class="btn btn-light" form="likeBookForm" id="submitInp" value="${submitValue}">
        </#if>

        <a href="/book" style="font-size: larger; margin-top: 10px; margin-left: 20px">to books</a>

    </div>


</section>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>