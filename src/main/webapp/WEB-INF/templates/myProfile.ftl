<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,300" type="text/css">
    <link rel="stylesheet" href="../../public/pages/css/styleProfile.css" type="text/css">


    <title>Hello, world!</title>
</head>
<body>


<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/logout">Logout <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/help">Help<span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-6 img">
            <img width="250" height="250" src="${user.avatar}"
                 alt="" class="img-rounded">
        </div>
        <div class="col-md-6 details">
            <blockquote>
                <h5>${user.nickname}</h5>
                <small><cite title="Source Title">${user.email}<i
                                class="icon-map-marker"></i></cite></small>
            </blockquote>
            <p>
                ${user.name} <br>
                ${user.country.name}<br>
                <br>
                <a href="/edit" style="color: black; font-size: large">edit</a>
            </p>
        </div>
    </div>
    <div class="row second">
        <a href="/favBooks" class="btn btn-primary" style="margin-top: 5px">favourite books</a>
    </div>
    <div class="row second">
        <a href="/favAuthors" class="btn btn-primary">favourite authors</a>
    </div>
    <div class="row second">
        <a href="/friends" class="btn btn-primary">friends</a>
    </div>
    <div class="row second">
        <a href="/followers" class="btn btn-primary">followers</a>
    </div>
    <div class="row second">
        <a href="/follows" class="btn btn-primary">follows</a>
    </div>
    <div class="row second">
        <a href="/messages" class="btn btn-primary">messages</a>
    </div>

</div>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>