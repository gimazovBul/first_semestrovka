<#include "base.ftl"/>




<#macro content>
<section class="l-page">

    <div class="efg" style="background-color: #ffffff;">

        <div class="nn6">
            <div class="form-row">
                <div class="form-group col-md-12">

                    <h2>List of favourite authors</h2>
                    <#if favAuthors??>
                        <#list favAuthors as author>

                            <div class="kniga">
                                <div class="card-group" style="background-color: rgba(197,197,197,0.16);">
                                    <#if author.image??>
                                        <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                            <img src="${author.image}" class="card-img" width="120"
                                                 height="140">

                                        </div>
                                    </#if>
                                    <div class="form-group col-md-11" style="background-color: rgba(197,197,197,0.0);">
                                        <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                            <a href="/author?authorId=${author.id}"><h4 class="text"
                                                                                  id="text">${author.name}</h4></a>

                                            &nbsp;

                                            <p class="card-text"><small class="text-muted">About</small></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </#list>
                    <#else>
                        You don't have any yet
                    </#if>
                </div>
            </div>
        </div>
    </div>
</section>
</#macro>
<@fav_list_macro/>

