<html>
<head>
    <link rel="stylesheet" href="../../public/pages/css/styleProfile.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row" style="margin-top: 15px">
        <div class="col-sm">
            <a href="/messages/im" class="btn btn-primary" style="margin-top: 5px; ">
                Incoming
            </a></div>
    </div>
    <div class="row" style="margin-top: 15px">
        <div class="col-sm">

            <a href="/messages/om" class="btn btn-primary" style="margin-top: 5px">
                Outgoing
            </a></div>
    </div>
    <div class="row" style="margin-top: 15px">
        <div class="col-sm">
            <a href="/messages/new" class="btn btn-primary" style="margin-top: 5px">
                New message
            </a></div>
    </div>
</div>
</div>

</body>
</html>