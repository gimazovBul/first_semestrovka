<html>
<head>

</head>
<body>
<#if books??>
    Books
    <#list books as book>
        <a href="/book?bookId=${book.id}">${book.name}</a>
    </#list>
</#if>
<#if author??>
    Author
    <a href="/author?authorId=${author.id}">${author.name}</a>
</#if>
</body>
</html>