<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="../../public/pages/css/main.css" type="text/css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</head>
<body>


<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="#">Library</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/profile">profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/searchAdv">search</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/users">users</a>
            </li>
        </ul>

    </div>
</nav>


<section class="l-page">


    <div class="efg" style="background-color: #ffffff;">

        <div class="nn6">

            <div class="form-row">
                <div class="form-group col-md-11">

                    <h2>authors</h2>
                    <div class="card-group">
                        <div class="form-row">
                            <#list authors as author>
                            <div class="form-group col-md-2">
                                <div class="card">
                                    <a href="author?authorId=${author.id}"> <img src="${author.image}" class="card-img-top" width="80"
                                         height="180"></a>
                                    <div class="card-footer">
                                       <a href="author?authorId=${author.id}"> <small class="text">${author.name}</small></a>
                                    </div>
                                </div>
                            </div>
                            </#list>

                        </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="nn6">
        <div class="form-row">
            <div class="form-group col-md-11">

                <h2>books</h2>
                <div class="card-group">
                    <div class="form-row">
                        <#list books as book>
                            <div class="form-group col-md-2">
                                <div class="card">
                                   <a href="/book?bookId=${book.id}"><img src="${book.image}" class="card-img-top" width="80"
                                         height="180"></a>
                                    <div class="card-footer">
                                        <a href="/book?bookId=${book.id}"> <small class="text">${book.name}</small></a>
                                    </div>
                                </div>
                            </div>
                        </#list>

                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="b123" id="ww2">

            <h5></h5>
            <h5></h5>


        </div>
    </div>

    </div>


</section>


<!-- jQuery first, then Popper.js, then Bootstrap JS -->
</body>
</html>