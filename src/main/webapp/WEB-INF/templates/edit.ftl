<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../public/pages/css/style.css">

    <title>Hello, world!</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>

</head>
<body>



<nav class= "navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<#if errMessages??>
    <div style="color: red">
        <#list errMessages as errmsg>
            ${errmsg}
        </#list>
    </div>
</#if>

<form action="/edit" method="post" enctype="multipart/form-data">
    <div class="abc" style="background-color: #ffffff;">

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="${user.email}" name="email"
                       pattern="[0-9a-z_-]+@[0-9a-z_-]+\.[a-z]{2,5}">
            </div>
            <div class="form-group col-md-6">
                <label for="nickname1">Nickname</label>
                <input name="nickname" type="text" class="form-control" id="nickname1" placeholder="${user.nickname}"
                       pattern="[0-9a-z_-]+" >
            </div>

        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="${user.name}">
            </div>

        </div>

        <div class="form-group">
            <select class="form-control" name="countryId">
                <option value="1">Russia</option>
                <option value="2">Belarus</option>
                <option value="3">China</option>
                <option value="4">Germany</option>
                <option value="5">Italy</option>
                <option value="6">Japan</option>
                <option value="7">Ukraine</option>
                <option value="8">United Kingdom</option>
                <option value="9">USA</option>
            </select>
        </div>

        <div class="custom-file">
            <label for="customFile">avatar</label>
            <label class="custom-file-label" for="customFile">Choose file</label>
            <input type="file" accept="image/jpeg, image/x-png" name="avatar" class="custom-file-input" id="customFile">
        </div>
        Delete profile
        <input style="margin-bottom: 5px" name="del" type="checkbox" value="true">

        <input type="submit" class="btn btn-secondary" style="margin-left: 50px;margin-top: 20px; display: inline-block">

    </div>
</form>
<#if saved??>
    <div style="color: green">
        ${saved}
    </div>
</#if>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>