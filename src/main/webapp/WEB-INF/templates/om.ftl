<#include "baseMessages.ftl"/>

<div class="nn6">
    <div class="form-group col-md-12">
        <h3>Outgoing</h3>
        <#if messages??>

    <#list messages as message>


        <div class="kniga">
            <div class="card-group" style="background-color: rgba(255,255,255,100);">

                <div class="card " style="background-color: rgba(255,255,255,100);">
                    <img src="${message.receiver.avatar}" class="card-img-top">

                </div>

                <div class="form-group col-md-11" style="background-color: rgba(255,255,255,100);">
                    <div class="card" style="background-color: rgba(255,255,255,100);">
                        <p class="card-text"><small class="text-muted"> ${message.receiver.nickname}</small></p>
                        <h5 class="text">${message.text}</h5>
                        <h5 class="text"> ${message.date?datetime}</h5>
                    </div>
                </div>
            </div>

        </div>
    </#list>
        <#else>
            You don't have any
        </#if>
    </div>


</div>
<@mes_list_macro/>