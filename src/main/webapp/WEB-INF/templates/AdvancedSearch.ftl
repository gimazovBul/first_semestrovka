<html>
<head>
    <link rel="stylesheet" href="../../public/pages/css/styleProfile.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../public/pages/css/styleFav.css" type="text/css">

    <script type="application/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="application/javascript">
        function search() {
            var type = $('#type');
            var name = $('#name');
            var ratingMin = $('#ratingMin');
            var ratingMax = $('#ratingMax');
            var res = $('#res');
            $.ajax({
                url: "/doSearchAdv",
                data: {
                    "type": type.val(),
                    "name": name.val(),
                    "ratingMin": ratingMin.val(),
                    "ratingMax": ratingMax.val()
                },
                type: "get",
                dataType: "json",
                success: function (resp) {
                    console.log(type.val());
                    res.html("");
                    for (var i = 0; i < resp.length; i++) {
                        res.append("<div class=\"kniga\">" +
                            "<div class=\"card-group\" style=\"background-color: rgba(197,197,197,0.16);\">" +
                            "<div class=\"card\" style=\"background-color: rgba(197,197,197,0.0);\">" +
                            "<img src=\"" + resp[i].image + "\" class=\"card-img\" width=\"120\" height=\"140\">" +
                            "</div>" +
                            "<div class=\"form-group col-md-11\" style=\"background-color: rgba(197,197,197,0.0);\">" +
                            "<div class=\"card\" style=\"background-color: rgba(197,197,197,0.0);\">");
                        if (type.val() === 'books') {
                            res.append("<a href=/book?bookId=" + resp[i].id + "><h4 class=\"text\">" + resp[i].name + "</h4></a>");
                        }
                        if (type.val() === 'authors') res.append("<a  href=/author?authorId=" + resp[i].id + "><h4 class=\"text\">" + resp[i].name + "</h4></a>");
                        res.append("</div>" + "</div>" + "</div>" + "</div>");
                    }
                }
            });
        }
    </script>
</head>
<body>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: rgba(0,0,62,0.71);">
    <a class="navbar-brand" href="/main">Library</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/profile">Profile <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>


<section class="l-page">

    <div class="efg" style="background-color: #ffffff;">

        <div class="nn6">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <h2>Advanced search</h2>
                    <div class="form-row">
                        <form action="/doSearchAdv" method="get" style="position:relative;margin-top: 20px"
                              onsubmit="search();return false;">
                            <select name="type" id="type">
                                <option value="books">books</option>
                                <option value="authors">authors</option>
                            </select>
                            <input type="text" id="name" name="name">
                            <select name="ratingMin" id="ratingMin">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <select name="ratingMax" id="ratingMax">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <input type="submit">
                        </form>
                    </div>

                    <div id="res">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


</body>
</html>