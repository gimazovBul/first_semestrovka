<#include "base.ftl"/>


<section class="l-page">

    <div class="efg" style="background-color: #ffffff;">

        <div class="nn6">
            <div class="form-row">
                <div class="form-group col-md-12">

                    <h2>List of favourite books</h2>
                    <#if favBooks??>
                        <#list favBooks as book>

                            <div class="kniga">
                                <div class="card-group" style="background-color: rgba(197,197,197,0.16);">
                                    <#if book.image??>
                                        <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                            <img src="${book.image}" class="card-img" width="120"
                                                 height="140">

                                        </div>
                                    </#if>
                                    <div class="form-group col-md-11" style="background-color: rgba(197,197,197,0.0);">
                                        <div class="card" style="background-color: rgba(197,197,197,0.0);">
                                            <a href="/book?bookId=${book.id}"><h4 class="text"
                                                                                  id="text">${book.name}</h4></a>

                                            &nbsp;

                                            <p class="card-text"><small class="text-muted">About</small></p>
                                            <p class="card-text"><small class="text-muted">${book.about}</small></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </#list>
                    <#else>
                        You don't have any yet
                    </#if>
                </div>
            </div>
        </div>
    </div>
</section>
<@fav_list_macro/>
